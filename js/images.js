const images = {
  gallery: [
    {
      street: {
        src: "images/photos/empena/menina/1.png",
        name: "Empena Menina",
        address: "Rua Margarida, 1026 <br/> Com vista para o Minhocão, São Paulo - SP",
        colors: [
          { name: "Roxo-vanguarda", hex: "#5f4888	" },
          { name: "Lírio-laranja", hex: "#fcb847" },
          { name: "Brisa Sublime", hex: " #cfc6e5" },
          { name: "Melancia", hex: "#dc5c58" },
          { name: "Quentão", hex: "#c4953e" },
          { name: "Terra Roxa", hex: "#bb7561" },
        ],
        link:
          "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
      },
      images: [
        {
          src: "images/photos/empena/menina/2.png",
          name: "Empena Menina",
          colors: [
            { name: "Roxo-vanguarda", hex: "#5f4888" },
            { name: "Lírio-laranja", hex: "#fcb847" },
          ],
          link:
            "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
        },
        {
          src: "images/photos/empena/menina/3.png",
          name: "Empena Menina",
          colors: [
            { name: "Brisa Sublime", hex: "#cfc6e5" },
            { name: "Lírio-laranja", hex: "#fcb847" },
          ],
          link:
            "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
        },
        {
          src: "images/photos/empena/menina/4.png",
          name: "Empena Menina",
          colors: [
            { name: "Melancia", hex: "#dc5c58" },
            { name: "Quentão", hex: "#c4953e" },
          ],
          link:
            "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
        },
        {
          src: "images/photos/empena/menina/5.png",
          name: "Empena Menina",
          colors: [
            { name: "Quentão", hex: "#c4953e" },
            { name: "Terra Roxa", hex: "#bb7561" },
          ],
          link:
            "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
        },
      ],
    },
    {
      street: {
        src: "images/photos/empena/peixe/1.png",
        name: "Empena Peixe",
        address: "Rua Margarida, 1026 <br/> Com vista para o Minhocão, São Paulo - SP",
        colors: [
          { name: "Lírio-laranja", hex: "#fcb847" },
          { name: "Brisa Sublime", hex: "#cfc6e5" },
          { name: "Verde-boêmia", hex: "#b1d6c0" },
          { name: "Melancia", hex: "#dc5c58	" },
          { name: "Curry", hex: "#d5a952" },
        ],
        link:
          "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
      },
      images: [
        {
          src: "images/photos/empena/peixe/2.png",
          name: "Empena Peixe",
          colors: [
            { name: "Lírio-laranja", hex: "#fcb847" },
            { name: "Brisa Sublime", hex: "#cfc6e5" },
          ],
          link:
            "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
        },
        {
          src: "images/photos/empena/peixe/3.png",
          name: "Empena Peixe",
          colors: [
            { name: "Verde-boêmia", hex: "#b1d6c0" },
            { name: "Melancia", hex: "#dc5c58" },
          ],
          link:
            "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
        },
        {
          src: "images/photos/empena/peixe/4.png",
          name: "Empena Peixe",
          colors: [
            { name: "Verde-boêmia", hex: "#b1d6c0" },
            { name: "Curry", hex: "#d5a952" },
          ],
          link:
            "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
        },
        {
          src: "images/photos/empena/peixe/5.png",
          name: "Empena Peixe",
          colors: [
            { name: "Quentão", hex: "#c4953e" },
            { name: "Terra Roxa", hex: "#bb7561" },
            { name: "Curry", hex: "#d5a952" },
          ],
          link:
            "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
        },
      ],
    },
    {
      street: {
        src: "images/photos/inspiracao/1/1.jpg",
        name: "Mural “Cecília”",
        author: "",
        address: "Indaiatuba - SP",
        colors: [
          { name: "Quentão", hex: "#c4953e" },
          { name: "Salgueiro", hex: "#b5ab7a" },
          { name: "Elefante", hex: "#a2a29d" },
          { name: "Cravo-da-índia", hex: "#795f4c" },
          { name: "Seriguela", hex: "#f8bb72" },
          { name: "Geleia de Goiaba", hex: "#d56831" },
        ],
        link:
          "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
      },
      images: [
        {
          src: "images/photos/inspiracao/1/2.png",
          name: "Mural “Cecília”",
          link:
            "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
          colors: [
            {
              name: "Quentão",
              hex: "#c4953e",
            },
            {
              name: "Geleia de goiaba",
              hex: "#d56831",
            },
          ],
        },
        {
          src: "images/photos/inspiracao/1/3.png",
          name: "Mural “Cecília”",
          link:
            "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
          colors: [
            { name: "Acampamento na Selva", hex: "#2d4c3b" },
            { name: "Elefante", hex: "#a2a29d" },
          ],
        },
      ],
    },
    {
      street: {
        src: "images/photos/inspiracao/2/1.jpg",
        name: "High Design",
        author: "Artista: Lanó <br/>",
        address: "Painel pintado na feira High Design 2019",
        colors: [
          { name: "Azul-cobalto", hex: "#3a477c" },
          { name: "Naturale", hex: "#f9d0b5" },
          { name: "Anos Setenta", hex: "#e38340" },
          { name: "Pimentão-verde", hex: "#93964a" },
        ],
        link:
          "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
      },
      images: [
        {
          link:
            "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
          src: "images/photos/inspiracao/2/2.png",
          name: "High Design",
          colors: [
            { name: "Naturale", hex: "#f9d0b5" },
            { name: "Pimentão-verde", hex: "#93964a" },
          ],
        },
        {
          link:
            "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
          src: "images/photos/inspiracao/2/3.png",
          name: "High Design",
          colors: [
            { name: "Anos Setenta", hex: "#e38340" },
            { name: "Azul-cobalto", hex: "#3a477c" },
          ],
        },
      ],
    },
    {
      street: {
        src: "images/photos/inspiracao/3/1.jpg",
        name: "Life is a Bit",
        author: "",
        address: "Painel pintado na SP Arte de 2019",
        link:
          "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
        colors: [
          { name: "Azul-cobalto", hex: "#3a477c" },
          {
            name: "Magenta",
            hex: "#c1537d",
          },
          { name: "Girassol", hex: "#fcd03c" },
        ],
        link:
          "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
      },
      images: [
        {
          src: "images/photos/inspiracao/3/2.png",
          name: "Life is a Bit",
          colors: [
            {
              name: "Magenta",
              hex: "#c1537d",
            },
            { name: "Azul-cobalto", hex: "#3a477c" },
            { name: "Girassol", hex: "#fcd03c" },
          ],
          link:
            "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
        },
        {
          src: "images/photos/inspiracao/3/3.png",
          name: "Life is a Bit",
          colors: [
            {
              name: "Magenta",
              hex: "#c1537d",
            },
            { name: "Azul-cobalto", hex: "#3a477c" },
          ],
          link:
            "https://loja.suvinil.com.br/criativa-para-pintar-novas-ideias?utm_source=SiteInstitucional&utm_medium=lp_criativa_speto_combinacoes&utm_campaign=lp_criativa_speto",
        },
      ],
    },
  ],
};
