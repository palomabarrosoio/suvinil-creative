$(document).ready(() => {
  let slideInterval = null;
  let slideTimeout = null;
  const slideProducts = () => {
    const wrap = document.getElementById("section-product__wrap");
    const element = document.getElementById("section-product__contrast");
    const elementName = document.getElementById("section-product__name");
    const mobile = window.innerWidth < 1024;
    let currentImage = 0;
    let htmlImages = "";
    let images = [
      { name: "Suvinil Criativa 800mL", src: "images/products/800ml.svg" },
      { name: "Suvinil Criativa 3.2L", src: "images/products/3L.svg" },
      { name: "Suvinil Criativa 5L", src: "images/products/5L.svg" },
      { name: "Suvinil Criativa 16L", src: "images/products/16L.svg" },
      { name: "Suvinil Criativa 18L", src: "images/products/18L.svg" },
    ];

    images.forEach((image) => {
      htmlImages += `
        <div class="section-product__image">
          ${
            window.innerWidth <= 1200
              ? `<svg
                width="126"
                height="158"
                viewBox="0 0 126 158"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <rect width="126" height="158" fill="#EFEFEF" />
              </svg>`
              : `<svg width="152" 
                  height="158" 
                  viewBox="0 0 152 158" 
                  fill="none" 
                fill="none"
                  fill="none" 
                fill="none"
                  fill="none" 
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <rect width="152" height="158" fill="#EFEFEF"/>
                </svg>`
          }
           <img src=${image.src} alt=${image.name} />
        </div>
    `;
      wrap.innerHTML = htmlImages;
    });

    const elementImages = document.querySelectorAll(".section-product__image");
    const slideImages = () => {
      clearTimeout(slideTimeout);
      if (element && images.length > 0) {
        elementName.innerHTML = images[currentImage].name;
        element.style.backgroundImage = `url(${images[currentImage].src})`;
        elementImages.forEach((_, index) => {
          if (index === currentImage) {
            elementImages[index].classList.add("active");
          } else {
            elementImages[index].classList.remove("active");
          }
        });
        currentImage = currentImage + 1;
        if (currentImage >= images.length) {
          currentImage = 0;
        }
      }
    };

    const goToSlide = (index) => {
      currentImage = index;
      if (!mobile) {
        window.clearInterval(slideInterval);
        slideImages();
        slideInterval = setInterval(slideImages, 4000);
      } else {
        slideImages();
      }
    };

    elementImages.forEach((element, index) => {
      element.addEventListener("click", () => {
        goToSlide(index);
      });
    });

    if (!mobile) {
      slideTimeout = setTimeout(slideImages);
      slideInterval = setInterval(slideImages, 4000);
    } else {
      slideImages();
    }
  };

  slideProducts();
  window.addEventListener("resize", () => {
    window.clearTimeout(slideTimeout);
    window.clearInterval(slideInterval);
    slideProducts();
  });
});
