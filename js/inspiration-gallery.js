$(document).ready(function () {
  const carousel = document.getElementById("section-inspiration__carousel");
  const navs = document.querySelectorAll(".section-inspiration__nav");
  const imgs = images.gallery.slice(2, 5);
  let carouselHtml = "";

  if (imgs.length <= 4) {
    navs.forEach((nav) => {
      nav.style.display = "none";
    });
  }

  imgs?.forEach((item, index) => {
    carouselHtml += `
    <div class="section-inspiration__carousel-item ${
      index === 2 ? "active" : ""
    }"
        onclick="clickInpiration(this,${index + 2})"
    >
      <img class="section-inspiration__carousel-image"
        src=${item.street.src} alt=${item.street.name} 
      /> 
    </div>`;
  });

  carousel.innerHTML = carouselHtml;

  const selectImg = imgs[0];
  setGalleryInspiration(selectImg, 2);

  new Glider(carousel, {
    slidesToShow: 4,
    slidesToScroll: 1,
    draggable: true,
    arrows: {
      prev: ".section-inspiration-prev",
      next: ".section-inspiration-next",
    },
    resizeLock: true,
  });
});
