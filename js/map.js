let map;
let queryParams = {
  address: "",
  q: "",
  lat: null,
  lng: null,
};
let current_page = null;
let last_page = null;
let next_page = null;
let total_page = null;

const api = axios.create({
  baseURL: "https://www.suvinil.com.br",
  headers: {
    "X-Requested-With": "XMLHttpRequest",
  },
});

const getShops = async (page) => {
  const response = await api.get(
    `/lojas?address=${queryParams?.address}&q=${queryParams?.q}
    ${queryParams?.lat ? `&lat=${queryParams?.lat}` : ""}
    ${queryParams?.lng ? `&lng=${queryParams?.lng}` : ""}
    ${page ? `&page=${page}` : ""}`
  );
  return response;
};

async function initMap(nav) {
  let page = null;
  let address = document.getElementById("map-address").value;
  let q = document.getElementById("map-shop").value;
  queryParams.address = address.trim().replace(/\s+/g, "+");
  queryParams.q = q.trim().replace(/\s+/g, "+");

  map = new google.maps.Map(document.getElementById("google-map"), {
    center: {
      lat: -15.7215857,
      lng: -48.0073978,
    },
    zoom: window.innerWidth < 768 ? 2 : 4,
  });

  if (nav === "prev" && current_page - 1 > 0) {
    page = current_page - 1;
  }

  if (nav === "next" && next_page <= total_page) {
    page = next_page;
  }

  const response = await getShops(page);
  const locations = response.data.data.data;
  const { meta } = response.data.data;

  current_page = meta.current_page;
  last_page = meta.last_page;
  next_page = meta.next_page;
  total_page = meta.total;

  let markers = locations.map((location, i) => {
    return new google.maps.Marker({
      position: { lat: location.latitude, lng: location.longitude },
    });
  });

  // Add a marker clusterer to manage the markers.
  let markerCluster = new MarkerClusterer(map, markers, {
    imagePath:
      "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
  });

  const list = document.getElementById("map-list");
  let htmlList = "";

  locations.forEach((loc) => {
    htmlList += `
      <li>
        <span class="section-map__list-name">${loc.name}</span>
        <span class="section-map__list-info">
          <p>${loc?.address.toLowerCase()}, ${loc?.neighborhood.toLowerCase()} - ${loc?.city.toLowerCase()} - 
            ${loc?.state} - 
            ${loc.zipCode.substr(0, 5)}-${loc.zipCode.substr(5, 3)}
          </p>
          <p>(${loc?.phone?.substr(0, 2)}) ${loc?.phone?.substr(
      2,
      4
    )}-${loc?.phone?.substr(6, 4)}</p>
        </span>
      </li>
    `;
  });

  list.innerHTML = `<ul>${htmlList}</ul>`;
}

const getLocation = () => {
  const setPosition = (position) => {
    queryParams.lat = position.coords.latitude;
    queryParams.lng = position.coords.longitude;
    initMap();
  };
  const getError = (error) => {
    switch (error.code) {
      case error.PERMISSION_DENIED:
        x.innerHTML = "Usuário rejeitou a solicitação de Geolocalização.";
        break;
      case error.POSITION_UNAVAILABLE:
        x.innerHTML = "Localização indisponível.";
        break;
      case error.TIMEOUT:
        x.innerHTML = "A requisição expirou.";
        break;
      case error.UNKNOWN_ERROR:
        x.innerHTML = "Algum erro desconhecido aconteceu.";
        break;
    }
  };

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(setPosition, getError);
  } else {
    console.error("Navegador não suporta geolocalização");
  }
};
