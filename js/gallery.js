$(document).ready(function () {
  const wrap = document.getElementById("section-gallery__wrap");
  let wrapHtml = "";

  images.gallery.slice(0, 2).forEach((item, indexParent) => {
    wrapHtml += `
      <div class="section-gallery__item">
        <div class="section-gallery__group section-gallery__group--first">
          <div class="section-gallery__images section-gallery__images--first">
            <img
              src=${item.street.src}
              alt=${item.street.name}
            />
          </div>
          <div class="section-gallery__group-caption">
            <span>${item.street.name}</span><br />
            ${item.street.address}
          </div>
          <div class="colors">
            <div class="colors-pallete">
              ${item.street.colors
                .map(
                  (color) =>
                    `<span style="background-color:${color.hex}"></span>`
                )
                .join("")}  
            </div>
            <div class="colors-pallete-name accordion-collors">
               ${item.street.colors
                 .map(
                   (color) => `
                 <div class="colors-item" style="background-color:${color.hex}">
                  <div class="colors-name">${color.name}</div>
                </div>`
                 )
                 .join("")}                
            </div>
            <button class="accordion accordion-bottom">
              <span>VER A PALETA DE CORES</span>
              <img src="images/icons/ic-arrow.svg" alt="" />
            </button>
          </div>
        </div>
        <div class="section-gallery__text">
          Confira abaixo as criativas artes do Speto e veja algumas ideias de como você pode usar as mesmas cores da rua na sua casa.
        </div>
        <div class="section-gallery__group section-gallery__group--miniature">
          <div class="section-gallery__group-images">
            ${item.images
              .map(
                (img, index) =>
                  `<div class="section-gallery__images section-gallery__images--miniature"
                  onclick="openModalGallery(${indexParent},${index})" 
                  id="${img.name}-${indexParent}-${index}"
                  data-parent="${indexParent}"
                  data-index="${index}"
                >
                  <img src="${img.src}" alt="${img.name}"/>                  
                  <div class="colors-pallete colors-pallete--mobile">
                    ${img.colors
                      .map(
                        (color) =>
                          `<span style="background-color:${color.hex}"></span>`
                      )
                      .join("")}  
                  </div>
                  <span class="expand"></span>
                </div>`
              )
              .join("")}
          </div>
          <div class="section-gallery__shop-button">
            Gostou dessa combinação?
            <a
              class="button-creative"
              href=${item.street.link}
              target="_blank"
              rel="noopener noreferrer"
            >
               COMPRE NA LOJA
            </a>
          </div>
        </div>
      </div>
    `;
  });

  setAccordionColors();
  wrap.innerHTML = wrapHtml;

  new Glider(wrap, {
    slidesToShow: 1,
    draggable: false,
    arrows: {
      prev: ".section-gallery-prev",
      next: ".section-gallery-next",
    },
    resizeLock: true,
  });
});
