const closeModalColor = () => {
  const element = document.getElementById("modal-creative__color-close");
  element.style.display = "none";
};

const showColorNameModal = (element) => {
  const content = document.getElementById("modal-creative__color-name");
  const colorName = element.getAttribute("data-color-name");
  const color = element.getAttribute("data-color-hex");
  let wrap = `
    <div class="colors-item" style="background-color:${color}" id="modal-creative__color-close">
      ${colorName} 
      <span class="creative-close creative-close--transparent" onclick="closeModalColor()">&times;</span>
    </div>
  `;
  content.innerHTML = wrap;
};

const modalNavigation = (nav) => {
  const modal = document.getElementById("modal-creative-body");
  const parent = Number(modal.getAttribute("data-parent"));
  const index = Number(modal.getAttribute("data-index"));
  const total = Number(modal.getAttribute("data-total"));
  if (nav === "prev" && index - 1 >= 0) {
    openModalGallery(parent, index - 1);
  }
  if (nav === "next" && index + 1 < total) {
    openModalGallery(parent, index + 1);
  }
};

const openModalGallery = (parent, index) => {
  const modal = document.getElementById("modal-creative");
  const modalBody = document.getElementById("modal-creative-body");
  const closeBtn = document.getElementById("modal-creative-close");
  const navPrev = document.getElementById("modal-creative__prev");
  const navNext = document.getElementById("modal-creative__next");
  const imgItem = images.gallery[parent].images[index];
  const totalImages = images.gallery[parent].images.length;

  modalBody.dataset.parent = parent;
  modalBody.dataset.total = totalImages;
  modalBody.dataset.next = index + 1 <= totalImages ? index + 1 : 0;
  modalBody.dataset.index = index;

  if (index === 0) {
    navPrev.style.display = "none";
  } else {
    navPrev.style.display = "block";
  }

  if (index + 1 === totalImages) {
    navNext.style.display = "none";
  } else {
    navNext.style.display = "block";
  }
  const htmlContent = `
    <div class="modal-creative__body-image">
      <img src=${imgItem.src} alt="${imgItem.name}"/>
      <div class="colors colors--left">
        <div class="modal-creative__color-name" id="modal-creative__color-name"></div>
        <div class="colors-pallete colors-pallete--modal">
        ${imgItem.colors
          .map(
            (color) => `
          <span
            style="background-color:${color.hex}" 
            onclick="showColorNameModal(this)"
            data-color-name="${color.name}"
            data-color-hex="${color.hex}"
          >
          </span>`
          )
          .join("")}  
        </div>
        <div class="colors-pallete-name accordion-collors">
          ${imgItem.colors
            .map(
              (color) => `
            <div class="colors-item" style="background-color:${color.hex}">
              <div class="colors-name">${color.name}</div>
            </div>`
            )
            .join("")}                
        </div>
      </div>
    </div>
    <div class="modal-creative__body-shop-button">
      Gostou dessa combinação?
      <a
        class="button-creative"
        href="${imgItem.link}"
        target="_blank"
        rel="noopener noreferrer"
      >
        COMPRE NA LOJA
      </a>
    </div>
  `;

  modalBody.innerHTML = htmlContent;
  modal.style.display = "block";
  window.onclick = function (event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  };
  closeBtn.onclick = function () {
    modal.style.display = "none";
  };
};

const setAccordionColors = () => {
  if (window.innerWidth < 1024) {
    const accordions = document.getElementsByClassName("accordion");
    for (let i = 0; i < accordions.length; i++) {
      accordions[i].addEventListener("click", function () {
        accordions[i].classList.toggle("active");
        let parent = accordions[i].parentElement;
        let panel = accordions[i].previousElementSibling;
        if (panel.style.display === "block") {
          panel.style.display = "none";
          parent.classList.remove("active");
        } else {
          panel.style.display = "block";
          parent.classList.toggle("active");
        }
      });
    }
  }
};

const setGalleryInspiration = (selectImg, parent) => {
  const gallery = document.getElementById("section-inspiration__gallery");
  let galleryHtml = "";
  setTimeout(() => (gallery.style.opacity = 0));

  galleryHtml += `
    <div  class="section-inspiration__wrap">
      <div class="section-inspiration__wrap-contrast">
        <img src=${selectImg.street.src} alt="${selectImg.street.name}"/>
          <div class="section-inspiration-caption">
            <span>${selectImg.street.name}</span><br />
            ${selectImg.street.author}
            ${selectImg.street.address}
          </div>
          <div class="colors">
            <div class="colors-pallete">
              ${selectImg.street.colors
                .map(
                  (color) =>
                    `<span style="background-color:${color.hex}"></span>`
                )
                .join("")}  
            </div>
            <div class="colors-pallete-name accordion-collors">
               ${selectImg.street.colors
                 .map(
                   (color) => `
                 <div class="colors-item" style="background-color:${color.hex}">
                  <div class="colors-name">${color.name}</div>
                </div>`
                 )
                 .join("")}                
            </div>
            <button class="accordion accordion-bottom">
              <span>VER A PALETA DE CORES</span>
              <img src="images/icons/ic-arrow.svg" alt="" />
            </button>
          </div>
      </div>
      <div class="section-inspiration__wrap-images">
         ${selectImg.images
           .map(
             (img, index) => `
            <div class="section-inspiration__wrap-images--miniature"
              onclick="openModalGallery(${parent}, ${index})" 
              id="${img.name}-${parent}-${index}"
            >
              <img src="${img.src}" alt="${img.name}"/>                  
              <div class="colors-pallete colors-pallete--mobile">
                ${img.colors
                  .map(
                    (color) => `
                  <span style="background-color:${color.hex}"></span>`
                  )
                  .join("")}  
              </div>
              <span class="expand"></span>
            </div>`
           )
           .join("")}
      </div>
    </div>
    <div class="section-inspiration__shop-button">
      Gostou dessa combinação?
      <a
        class="button-creative"
        href="${selectImg.street.link}"
        target="_blank"
        rel="noopener noreferrer"
      >
        COMPRE NA LOJA
      </a>
    </div>
    `;

  setTimeout(() => {
    gallery.style.opacity = 1;
    gallery.innerHTML = galleryHtml;
    setAccordionColors();
  }, 500);
};

const clickInpiration = (element, parent) => {
  const elementImages = document.querySelectorAll(
    ".section-inspiration__carousel-item"
  );
  const selectImg = images.gallery[parent];
  elementImages.forEach((item) => {
    item.classList.remove("active");
  });

  setTimeout(() => element.classList.add("active"), 100);
  setGalleryInspiration(selectImg, parent);
};
