# API - Lojas

https://www.suvinil.com.br/lojas

## Headers

X-Requested-With: XMLHttpRequest,

## Parâmetros

- address : Filtra pelo endereço, pode passar qualquer texto

- q : Filtra pelo nome da loja

- lat e lng : Filtra a partir da localização informada.

- page : Navega até a página informada
